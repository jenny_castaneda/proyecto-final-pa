package com.web.app;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.*;

import com.google.appengine.api.utils.SystemProperty;

import java.sql.*;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class UsersServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String url = null;
		if (SystemProperty.environment.value() ==
			SystemProperty.Environment.Value.Production) {
			// Connecting from App Engine.
			// Load the class that provides the "jdbc:google:mysql://"
			// prefix.
			try {
				Class.forName("com.mysql.jdbc.GoogleDriver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			url =
			"jdbc:google:mysql://genial-caster-784:bd-proyecto-final-pa/proyecto?user=root";
		} else {
			 // Connecting from an external network.
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			url = "jdbc:mysql://173.194.83.76:3306/proyecto?user=root";
		}

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			ResultSet rs = conn.createStatement().executeQuery(
			"SELECT * FROM users");
			
			resp.setContentType("text/html");
			resp.getWriter().println("<html>");
			resp.getWriter().println("<head>");
			resp.getWriter().println("</head>");
			resp.getWriter().println("<body>");
			
			resp.getWriter().println("<table border='1'>");
			resp.getWriter().println("<tr>");
			resp.getWriter().println("<td><b>Nombre</b></td>");
			resp.getWriter().println("<td><b>Usuario</b></td>");
			resp.getWriter().println("</tr>");
			
			while (rs.next()) 
			{
		    	resp.getWriter().println("<tr>");
				resp.getWriter().println("<td>" + rs.getString(2) + "</td>");
				resp.getWriter().println("<td>" + rs.getString(3) + "</td>");
				resp.getWriter().println("</tr>");
			}
			
			resp.getWriter().println("</table>");
			
			resp.getWriter().println("<p><a href='/user'>Crear usuario</a></p><p><a href='/'>Regresar</a></p>");
			
			resp.getWriter().println("</body>");
			resp.getWriter().println("</html>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
}
