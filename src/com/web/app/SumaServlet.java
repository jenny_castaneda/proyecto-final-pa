package com.web.app;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class SumaServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		PrintWriter pw= resp.getWriter();
		resp.setContentType("text/plain");
		
		String num=req.getParameter("suma");
		String sum=req.getParameter("sumando");
		pw.println("Su respuesta es: ");
		pw.println(Double.parseDouble(num)+Double.parseDouble(sum));
	
	}
}
