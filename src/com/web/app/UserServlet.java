package com.web.app;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.*;

import com.google.appengine.api.utils.SystemProperty;

import java.sql.*;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class UserServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("text/html");
		resp.getWriter().println("<html>");
		resp.getWriter().println("<head>");
		resp.getWriter().println("</head>");
		resp.getWriter().println("<body>");
		resp.getWriter().println("<form action='/user' method='POST'>");
		resp.getWriter().println("<label for='name'>Nombre:</label>");
		resp.getWriter().println("<input type ='text' name='name'><br />");
		resp.getWriter().println("<label for='username'>Usuario:</label>");
		resp.getWriter().println("<input type ='text' name='username'><br />");
		resp.getWriter().println("<label for='password'>Password:</label>");
		resp.getWriter().println("<input type ='password' name='password'><br />");
		
		resp.getWriter().println("<input type ='submit' value='Insertar'> ");
		resp.getWriter().println("</form>");
		resp.getWriter().println("</body>");
		resp.getWriter().println("</html>");
			
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String name=req.getParameter("name");
		String username=req.getParameter("username");
		String password=req.getParameter("password");
		
		String url = null;
		if (SystemProperty.environment.value() ==
			SystemProperty.Environment.Value.Production) {
			// Connecting from App Engine.
			// Load the class that provides the "jdbc:google:mysql://"
			// prefix.
			try {
				Class.forName("com.mysql.jdbc.GoogleDriver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			url =
			"jdbc:google:mysql://genial-caster-784:bd-proyecto-final-pa/proyecto?user=root";
		} else {
			 // Connecting from an external network.
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			url = "jdbc:mysql://173.194.83.76:3306/proyecto?user=root";
		}

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			int rs = conn.createStatement().executeUpdate(
			"INSERT INTO `users` (`name`, `username`, `password`) VALUES ('"+name+"', '"+username+"', '"+password+"')");
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		resp.setContentType("text/html");
		resp.getWriter().println("<html>");
		resp.getWriter().println("<head>");
		resp.getWriter().println("</head>");
		resp.getWriter().println("<body>");
		resp.getWriter().println("<p>Usuario insertado exitosamente!</p>");
		resp.getWriter().println("<p><a href='/users'>Regresar</a></p>");
		resp.getWriter().println("</body>");
		resp.getWriter().println("</html>");
	}
}
